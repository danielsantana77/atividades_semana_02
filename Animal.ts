type ClasseAnimal = 'Mamífero' | 'Ave';

export class Animal {
  protected nome: string;
  protected idade: number;
  private classe: ClasseAnimal;

  constructor(nome: string, idade: number, classe: ClasseAnimal) {
    this.nome = nome;
    this.idade = idade;
    this.classe = classe;
  }

  getIdade() {
    return this.idade;
  }

  alimentar() {
    console.log(`${this.nome} está se alimentando`);
  }

  reproducao(nome?:string|number) {
    console.log('Muitas formas de reprodução');
  }

  chama(){
      return this.classe
  }
}