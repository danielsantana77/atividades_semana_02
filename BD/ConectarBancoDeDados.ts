export abstract class ConectarBancoDeDados {
    private host: string
    private port: number
    private username: string
    private password: string
    private databaseName: string

    constructor(
        host: string,
        port: number,
        username: string,
        password: string,
        databaseName: string,
    ) {
        this.host= host
        this.username = username
        this.databaseName = databaseName
        this.password = password
        this.port = port
    }

      abstract conectar():void
}