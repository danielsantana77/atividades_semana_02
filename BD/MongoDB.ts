import { ConectarBancoDeDados } from "./ConectarBancoDeDados";

export class MongoDB extends ConectarBancoDeDados implements ConectarBancoDeDados {

    private sslActive: boolean

    constructor(
        host: string,
        port: number,
        username: string,
        password: string,
        databaseName: string,
    ) {
        super(host, port, username, password, databaseName);
        this.sslActive = false;
        

    }


    conectar(): void {
        console.log("Conectando o banco MongoDB");
        console.log(JSON.stringify(this, null, 2));

    }

}