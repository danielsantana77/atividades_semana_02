import { ConectarBancoDeDados } from "./ConectarBancoDeDados";

export class PostgreSQL extends ConectarBancoDeDados {
  
    private schema: string;
  
    constructor(
      host: string,
      port: number,
      username: string,
      password: string,
      databaseName: string,
      schema: string,
    ) {
      super(host, port, username, password, databaseName);
      this.schema = schema;
    }
  
    conectar(): void {
      console.log('Conectando o banco PostgreSQL..., INFO:');
      console.log(JSON.stringify(this, null, 2));
    }
  }
  