import {Mamifero} from './Mamifero'
export class Cachorro extends Mamifero {
    constructor() {
      super("nome", 10);
    }
  
    getIdade(): number {
      return this.idade * 7;
    }
    
    reproducao(): void {
        console.log("Solta os cachorros");
        
    }

  }