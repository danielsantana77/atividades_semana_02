class Circulo {
    raio: number
    static readonly pi: number = 3.14

    constructor(raio: number) {
        this.raio = raio
    }
    static calcularArea(raio: number) {
        return Circulo.pi * raio * raio
    }
}

const c = new Circulo(1)
console.log(Circulo.pi);
console.log(Circulo.calcularArea(5));
console.log(c);


