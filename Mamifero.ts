import { Animal } from "./Animal";


export class Mamifero extends Animal {
    constructor(nome: string, idade: number) {
      super(nome, idade, 'Mamífero');
    }
  
    reproducao(nome?: string) {
      console.log('Reprodução sexuada');
    }
  }