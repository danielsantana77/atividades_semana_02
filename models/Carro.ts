import { Veiculo, CambioVeiculo } from "./Veiculo";

export class Carro extends Veiculo {

    quantidadePortas: number;


    constructor(
        modelo: string,
        ano: number,
        cor: string,
        motor: string,
        cambio: CambioVeiculo,
        velocidade: number,
        quantidadePortas: number,
        ) {
            
        super(modelo, ano, cor, motor, cambio, velocidade);
        if (quantidadePortas < 2) {
            throw new Error("Esse Carro é inválido")
        } else {
            this.quantidadePortas = quantidadePortas
        }
     
    }


   
   

}

export class Motor {
    static modelo:string = "teste"


}


