import { Carro } from "./Carro";
import { CambioVeiculo } from "./Veiculo";


export class Uno extends Carro {

    temEscada:boolean

    constructor(
        ano: number,
        cor: string,
        motor: string,
        quantidadePortas: number,
        temEscada:boolean) {

        super("Uno", ano, cor, motor, "Manual", 0, quantidadePortas)
        this.temEscada = temEscada

    }


}

