import { IVeiculo, VeiculoType } from "../Interface/IVeiculo"

export type CambioVeiculo = "Automático" | "Manual"

export class Veiculo  implements VeiculoType{
    private modelo!: string
    private ano!: number
    private cor!: string
    private motor!: string
    private cambio!: CambioVeiculo
    private velocidade!: number

    constructor(
        modelo: string,
        ano: number,
        cor: string,
        motor: string,
        cambio: CambioVeiculo,
        velocidade: number) {

        this.modelo = modelo
        this.cor = cor
        this.ano = ano
        this.cambio = cambio
        this.motor = motor
        this.velocidade = 0
    }

    public ligar() {
        console.log("Carro Ligado")
    }

    public desligar():void {
        console.log("Carro Desligado");
    }


     acelerar(velocidade=15){
        this.velocidade += velocidade
    }

    public frear(){
        
    }
    
    public getModelo():string{
        return this.modelo
    }
     

}